package testcases;

import java.awt.AWTException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lib.selenium.PreAndPost;
import pages.LoginPage;

public class TC_ServiceConsole extends PreAndPost {

	@BeforeTest
	public void setData() {
		testCaseName = "TC_ServiceConsole";
		testDescription = "ServiceConsole";
		authors = "viji";
		category = "smoke";
		nodes = "Service";
	}
		
	@Test
	public void login() throws InterruptedException, AWTException {
		new LoginPage(driver, test)
		.typeUserName("nupela@testleaf.com")
		.typePassword("Bootcamp$123")
		.clickLogIn()
		.clickAppLauncer()
		.clickViewAll()
		.clickService()
		.selectDropDown()
		.clickFiles()
		.clickLastestModifyItem()
		.clickOnShare()
		.searchUser()
		.selectName()
		.verifyError()
		.clickCancel()
		.clickDropForNewFile()
		.selectDelete()
		.confirmDelete()
		.verifySuccessMsg()	
		;
		
	}
}
