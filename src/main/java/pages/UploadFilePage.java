package pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class UploadFilePage extends PreAndPost{
	public UploadFilePage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public UploadFilePage uploadfile() throws AWTException {
		sleep(10000);
		StringSelection stringSelection = new StringSelection("C:\\Users\\Learn\\Downloads\\Photos\\Viji Photo resized\\Passport size photo.jpg");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		// Paste it using Robot class
				Robot robot = new Robot();

				// Enter to confirm it is uploaded
				robot.keyPress(KeyEvent.VK_CONTROL);
				robot.keyPress(KeyEvent.VK_V);

				robot.keyRelease(KeyEvent.VK_V);
				robot.keyRelease(KeyEvent.VK_CONTROL);

				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				sleep(5000);
		return this;
	}
	
	public ServiceConsolePage clickDone() {
		WebElement doneClick = locateElement("xpath","//span[text()='Done']");
		Actions builder = new Actions(driver);
		builder.moveToElement(doneClick).click().perform();
		sleep(3000);
		return new ServiceConsolePage(driver, test);
	}
	

}
