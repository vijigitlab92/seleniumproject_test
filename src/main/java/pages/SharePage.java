package pages;

import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class SharePage extends PreAndPost{
	public SharePage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public SharePage searchUser() {
		click(locateElement("xpath","//input[@title='Search People']"));
		return this;
	}
	public SharePage selectName() {
	   click(locateElement("xpath","//div[text()='Derrick Dsouza']"));
	   return this;
}
	public SharePage verifyError() {
    	String errorMessage = locateElement("xpath","//li[@class='form-element__help']").getText();
		Assert.assertEquals(errorMessage, "Can't share file with the file owner.");
		return this;
	}
	
	public ServiceConsolePage clickCancel() {
		click(locateElement("xpath","(//span[text()='Cancel'])[2]"));
		return new ServiceConsolePage(driver, test);
	}
	
	public ServiceConsolePage closeItemDialog() {
		click(locateElement("xpath","//button[@title='Close']"));
		return new ServiceConsolePage(driver, test);
	}
}
