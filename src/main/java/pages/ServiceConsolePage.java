package pages;

import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class ServiceConsolePage extends PreAndPost{
	public ServiceConsolePage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public ServiceConsolePage selectDropDown() {
		sleep(5000);
		click(locateElement("xpath","//button[@title='Show Navigation Menu']"));
		
		return this;
	}

	public ServiceConsolePage clickFiles() {
		click(locateElement("xpath","//span[text()='Files']"));
		return this;
	}
	
	public UploadFilePage clickUpload() {
		WebElement uploadButton = locateElement("xpath","//div[text()='Upload Files']");
		Actions builder = new Actions(driver);
		builder.moveToElement(uploadButton).click().perform();
        
        return new UploadFilePage(driver, test);
	}
	
	public ServiceConsolePage selectDropDownNewFile() {
		click(locateElement("xpath","//div[@data-aura-class='forceVirtualAction']/a"));
		sleep(3000);
		return this;
	}
	
	public FileDetailsPage selectViewFile() {
		click(locateElement("xpath","//a[@title='View File Details']"));
		return new FileDetailsPage(driver, test);
	}
	
	public ServiceConsolePage clickLastestModifyItem() {
		sleep(2000);
		click(locateElement("xpath","(//a[contains(@class,'button--icon-border-filled')])[1]"));
		return this;
	}
	
	public SharePage clickOnShare() {
		click(locateElement("xpath","//a[@title='Share']"));
		return new SharePage(driver, test);
	}
	
	public ServiceConsolePage clickDropForNewFile() {
		click(locateElement("xpath","//li[@class='oneActionsDropDown']//a"));
		return this;
	}
	
	public ServiceConsolePage selectDelete() {
		click(locateElement("xpath","//a[@title='Delete']"));
       return this;
	}
	
	public ServiceConsolePage confirmDelete() {
		click(locateElement("xpath","//span[text()='Delete']"));
		sleep(3000);
		  return this;
}
		
	public ServiceConsolePage verifySuccessMsg() {
		String text = locateElement("xpath","//span[@class='toastMessage slds-text-heading--small forceActionsText']").getText();
		System.out.println(text);
		  return this;
	}
			
			
}
