package pages;

import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;

import lib.selenium.PreAndPost;

public class FileDetailsPage extends PreAndPost{
	public FileDetailsPage(EventFiringWebDriver driver, ExtentTest test) {
		this.driver = driver;
		this.test = test;
	}
	
	public FileDetailsPage verifyFileName() {
		String fileName = locateElement("xpath","//div[text()='File']/following::span").getText();
		System.out.println(fileName);
		Assert.assertEquals(fileName, "Passport size photo");
		return this;
	}
	
	public FileDetailsPage verifyFileExt() {
		String fileExtension = locateElement("xpath","//span[@title='File Extension']/following::span").getText();
		System.out.println(fileExtension);
		Assert.assertEquals(fileExtension, "jpg");
		return this;
	}
	
	public ServiceConsolePage closeFileWinTab() {
		click(locateElement("xpath","//div[@class='close slds-col--bump-left slds-p-left--none slds-context-bar__icon-action ']"));
		return new ServiceConsolePage(driver, test);
	}
}
